package;

import haxe.ds.ReadOnlyArray;
import haxe.ds.Either;

private typedef NDimensionFieldPart = Either<Array<OthelloPiece>, Array<NDimensionFieldPart>>;
typedef ReadOnlyNDimensionFieldPart = Either<ReadOnlyArray<OthelloPiece>, ReadOnlyArray<ReadOnlyNDimensionFieldPart>>;
typedef ReadOnlyNDimensionField = {
    final field: ReadOnlyNDimensionFieldPart;
    final n: Int;
}

class NDimensionField {
    final field: NDimensionFieldPart;
    final n: Int;

    public function new(dimension: Int) {
        n = dimension;
        field = makeField(dimension);
        initialPutColor();
    }

    public function traceField(?field: NDimensionFieldPart, ?indent: Int = 0) {
        if (field == null) field = this.field;
        switch (field) {
            case Left(pieces):
                trace([for (_ in 0...indent) ' '].join('') + pieces);
            case Right(field):
                trace([for (_ in 0...indent) ' '].join('') + '[');
                for (i in field) {
                    traceField(i, indent + 4);
                }
                trace([for (_ in 0...indent) ' '].join('') + ']');
        }
    }

    static function makeField(dimension: Int):NDimensionFieldPart {
        return if (dimension == 1) {
            Left([for (i in 0...8) None]);
        } else {
            Right([for (i in 0...8) makeField(dimension - 1)]);
        };
    }

    public function getField():ReadOnlyNDimensionField {
        return {
            field: cast field,
            n: n
        };
    }

    function initialPutColor() {
        final toPuts = product([for (_ in 0...n) [3, 4]]);
        for (pos in toPuts) {
            if (sum(pos) % 2 == 0) {
                setPieceOrThrow(pos, Color(White));
            } else {
                setPieceOrThrow(pos, Color(Black));
            }
        }
    }

    public function isValidPosArray(pos: Array<Int>, ?throwError:Bool=false) {
        if (pos.length != n) {
            if (throwError) throw 'not match pos.length == n';
            else return false;
        }

        for (i in pos) {
            if (i > 7 || i < 0) return false;
        }
        
        return true;
    }

    public function getPieceOrThrow(pos: Array<Int>) {
        isValidPosArray(pos, true);

        var current = field;
        for (i in pos.slice(0, pos.length - 1)) {
            switch (current) {
                case Left(_):
                    throw 'Unreachable';
                case Right(subField):
                    current = subField[i];
            }
        }
        switch (current) {
            case Left(pieces):
                return pieces[pos[pos.length - 1]];
            case Right(_):
                throw 'Unreachable';
        }
    }
    
    public function setPieceOrThrow(pos: Array<Int>, piece: OthelloPiece) {
        isValidPosArray(pos, true);

        var current = field;
        
        for (i in pos.slice(0, pos.length - 1)) {
            switch (current) {
                case Left(_):
                    throw 'Unreachable';
                case Right(subField):
                    current = subField[i];
            }
        }
        switch (current) {
            case Left(pieces):
                pieces[pos[pos.length - 1]] = piece;
            case Right(_):
                throw 'Unreachable';
        }
    }

    public function judgeLines() {
        final result = product([for (_ in 0...n) [1, -1, 0]]);
        result.pop();  // [0, 0, ...] を削除
        return result;
    }

    static function product<T>(args: Array<Array<T>>) {
        if (args.length == 1) {
            return args[0].map(n -> [n]);
        }

        final rest = args.slice(1);
        final rProd = product(rest);

        return Lambda.flatten(
            args[0].map(
                m -> rProd.map(
                    n -> [m].concat(n)
                )
            )
        );

        throw '';
    }

    function sum(arr: Array<Int>) {
        var sum = 0;
        for (i in arr) sum += i;
        return sum;
    }
}
