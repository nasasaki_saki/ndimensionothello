package ;

import OthelloColor;
import OthelloPiece;

using OthelloTools;

class OneDimensionOthello {
    final field: Array<Array<OthelloPiece>>;
    var currentColor: OthelloColor;

    public function new() {
        field = [
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, Color(Black), Color(White), None, None, None],
            [None, None, None, Color(White), Color(Black), None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None, None],
        ];
        currentColor = Black;
    }

    public function getField() {
        return field;
    }

    public function printField() {
        final fieldStrs: Array<String> = [];
        trace(' : 0| 1| 2| 3| 4| 5| 6| 7|');
        for (yIndex in 0...field.length) {
            final y = field[yIndex];
            fieldStrs.push("");

            for (x in y) {
                switch (x) {
                    case None:
                        fieldStrs[yIndex] += '  |';
                    case Color(White):
                        fieldStrs[yIndex] += '○ |';
                    case Color(Black):
                        fieldStrs[yIndex] += '● |';
                    }
            }

            trace(yIndex + ':' + fieldStrs[yIndex]);
        }
    }

    function reversePoint(pos: Array<Int>) {
        field[pos[0]][pos[1]] = Color(field[pos[0]][pos[1]].colorOrThrow().reversed());
    }

    function doReverseLine(toReverse: Array<Array<Int>>) {
        for (pos in toReverse) {
            reversePoint(pos);
        }
    }

    function reverseLine(origin: Array<Int>, grow: Array<Int>, ?checkOnly:Bool = false):Int {
        final originColor = field[origin[0]][origin[1]].colorOrThrow();
        final reverseColor = originColor.reversed();

        final current = origin.copy();
        final toReverseTmp: Array<Array<Int>> = [];

        while (!(current[0] >= 8 || current[1] >= 8 || current[0] < 0 || current[1] < 0)) {
            current[0] += grow[0];
            current[1] += grow[1];
            switch (field[current[0]][current[1]]) {
                case None:
                    return 0;
                case Color(color):
                    if (color.equals(reverseColor)) {
                        toReverseTmp.push(current.copy());
                    } else {
                        if (!checkOnly) doReverseLine(toReverseTmp);
                        return toReverseTmp.length;
                    }
            }
        }

        return 0;
    }

    public function putAndReverse(pos: Array<Int>, ?checkOnly:Bool=false):Bool {
        switch (field[pos[0]][pos[1]]) {
            case Color(_):
                return false;
            case None:
                field[pos[0]][pos[1]] = Color(currentColor);
        }

        var sum = 0;
        sum += reverseLine(pos, [ 1,  0], checkOnly);  // 右
        sum += reverseLine(pos, [-1,  0], checkOnly);  // 左
        sum += reverseLine(pos, [ 0,  1], checkOnly);  // 下
        sum += reverseLine(pos, [ 0, -1], checkOnly);  // 上

        sum += reverseLine(pos, [ 1, -1], checkOnly);  // 右上
        sum += reverseLine(pos, [ 1,  1], checkOnly);  // 右下
        sum += reverseLine(pos, [-1, -1], checkOnly);  // 左上
        sum += reverseLine(pos, [-1,  1], checkOnly);  // 左下

        if (sum == 0) {
            field[pos[0]][pos[1]] = None;
            return false;
        } else {
            return true;
        }
    }
}
