package;

class OthelloTools {
    public static function reversed(color: OthelloColor):OthelloColor {
        return switch (color) {
            case White:
                Black;
            case Black:
                White;
        }
    }

    public static function colorOrThrow(piece: OthelloPiece):OthelloColor {
        switch(piece) {
            case None:
                throw 'piece is None.';
            case Color(color):
                return color;
        }
    }
}
