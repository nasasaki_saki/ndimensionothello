package ;

class Main {
    public static function main() {
        trace("hello, world!");
        // final othello2d = new NDimensionOthello(2);
        final othello3d = new NDimensionOthello(3);
        // othello2d.traceField();
        othello3d.traceField();
        // othello2d.putAndReverse([3, 5]);
        trace('--------------------------');
        othello3d.putAndReverse([3, 3, 5]);
        othello3d.traceField();
        othello3d.getField();
    }
}
