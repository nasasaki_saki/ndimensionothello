package;

import OthelloColor;
import NDimensionField;

using OthelloTools;

class NDimensionOthello {
    final field: NDimensionField;
    var currentColor: OthelloColor;

    public function new(dimension: Int, ?firstColor:OthelloColor = Black) {
        field = new NDimensionField(dimension);
        currentColor = firstColor;
    }

    public function traceField() {
        field.traceField();
    }

    public function getField() {
        return field.getField();
    }

    function initialPut() {
        final whites2d = [[3, 3], [4, 4]];
        final blacks2d = [[3, 4], [4, 3]];

        final whites3d = [[3, 3, 4], [3, 4, 3], [4, 3, 3], [4, 4, 4]];
        final blacks3d = [[3, 3, 3], [3, 4, 4], [4, 3, 4], [4, 4, 3]];
    }

    function reversePoint(pos: Array<Int>) {
        field.setPieceOrThrow(pos, Color(field.getPieceOrThrow(pos).colorOrThrow().reversed()));
    }

    function growPosition(origin: Array<Int>, grow: Array<Int>) {
        if (origin.length != grow.length) {
            throw 'cant grow position-array with different length grow-array.';
        }
        for (i in 0...origin.length) {
            origin[i] += grow[i];
        }
    }

    function doReverseLine(toReverse: Array<Array<Int>>) {
        for (pos in toReverse) {
            reversePoint(pos);
        }
    }

    function reverseLine(origin: Array<Int>, grow: Array<Int>, originColor: OthelloColor, ?checkOnly:Bool=false) {
        field.isValidPosArray(origin, true);
        field.isValidPosArray(origin, true);
        final reverseColor = originColor.reversed();

        final current = origin.copy();
        final toReverseIndexes: Array<Array<Int>> = [];
        
        while (field.isValidPosArray(current)) {
            growPosition(current, grow);
            switch (field.getPieceOrThrow(current)) {
                case None:
                    return 0;
                case Color(color):
                    if (color.equals(reverseColor)) {
                        toReverseIndexes.push(current.copy());
                    } else {
                        if (!checkOnly) doReverseLine(toReverseIndexes);
                        return toReverseIndexes.length;
                    }
            }
        }

        return 0;
    }

    public function putAndReverse(pos: Array<Int>, ?checkOnly:Bool=false):Bool {
        switch (field.getPieceOrThrow(pos)) {
            case Color(_):
                return false;
            case None:
        }

        var sum = 0;
        for (grow in field.judgeLines()) {
            sum += reverseLine(pos, grow, currentColor, checkOnly);
        }

        if (sum == 0) {
            return false;
        } else {
            field.setPieceOrThrow(pos, Color(currentColor));
            currentColor = currentColor.reversed();
            return true;
        }
        return true;
    }
}
